def func2_9(filename):
    try:
        with open(filename) as file_object:
            to_anotherFile = ' '.join(list([x for x in file_object.readlines() if x.strip()[0] !='#']))
    except FileNotFoundError:
        pritn('Pliku nie znaleziono')
    
    new_file = 'copy_tofile.txt'
    with open(new_file,'w') as file_obj:
        file_obj.write(to_anotherFile)