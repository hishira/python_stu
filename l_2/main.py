from functions import func2_9

#2.9
#func2_9('line.txt')

#2.10
many = '''o co    
wam chodzi i co
chcecie zrobic 
nie wiem
        tak i co
jak sie kochamy 
    ha ha ha'''
arr = many.split()
print(len(arr))

#2.11
print('_'.join('word'))

#2.12
first = ''.join(x[0] for x in arr)
print(first)
last = ''.join(x[-1] for x in arr)
print(last)

#2.13
suma = sum(len(x) for x in arr)
print(suma)

#2.14
minimum = min(len(x) for x in arr)
maximum = max(len(x) for x in arr)
print(minimum)
print(maximum)

#2.15
lista = [5,4,5,64,46564,756,45645,34545,6456]
nazwa = ' '.join(map(str,lista))
print(nazwa)

#2.16
many = '''o co    
wam GvR i co
chcecie zrobic 
GvR wiem
        tak i co
jak sie kochamy 
    GvR ha ha'''
print('Przed:',many)
many = many.replace('GvR','Guido van Rossum')
print('Po:',many)

#2.17
print('Przed posortowaniem',arr)
print('Posortowanie alfabetycznie',sorted(arr))
print('Posortowanie po długości',sorted(arr,key=len))
liczba = 123101928300000018203180102102031023010230120310923182031020000109203102391020120000012
print(str(liczba).count('0'))
liczby = [5,4,56,45,543,645,774,45,345,66,4,56,775]
nazwa = ' '.join(str(x).zfill(3) for x in liczby)
print(nazwa)