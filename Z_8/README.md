# Zestaw 8
Autor: Michał Skorupa
Jak narazie zrobione zadania to 8.1, 8.3 i 8.4

## ZADANIE 8.1

Zbadać problem szukania rozwiązań równania liniowego postaci a * x + b * y + c = 0. Podać
specyfikację problemu. Podać algorytm rozwiązania w postaci listy kroków, schematu blokowego,
drzewa. Podać implementację algorytmu w Pythonie w postaci funkcji solve1(), która rozwiązania
wypisuje w formie komunikatów.

```python
def solve1(a, b, c):
    """Rozwiązywanie równania liniowego a x + b y + c = 0."""
    pass
```

### Usage
```python
from random import uniform #Losowanie liczb typu float
x = uniform(... , ...)
y = uniform(..., ...)
```

## ZADANIE 8.3
Obliczyć liczbę pi za pomocą algorytmu Monte Carlo. Wykorzystać losowanie punktów z kwadratu z wpisanym kołem. Sprawdzić zależność dokładności wyniku od liczby losowań. Wskazówka: Skorzystać z modułu random.

```python
def calc_pi(n=100):
    """Obliczanie liczby pi metodą Monte Carlo.
    n oznacza liczbę losowanych punktów."""
    pass
```

## Zadanie 8.4
Bezsensu, z powodu rekurencji częściej się rozsypuje niż coś oblicza

### Python
```python
def ackerman(n: int, p: int) :
    """Funkcja Ackermanna, n i p to liczby naturalne.
    Zachodzi A(0, p) = 1, A(n, 1) = 2n, A(n, 2) = 2 ** n,
    A(n, 3) = 2 ** A(n-1, 3).
    """
    if (n,p) in ackerman_tab:
        return ackerman_tab[(n,p)]
    else:
        if n == 0:
            r = p+1
        if n > 0 and p == 0:
            r = ackerman(n-1,1)
        if n > 0 and p > 0:
            r = ackerman(n-1,ackerman(n,p-1))
        ackerman_tab[(n,p)] = r
        return r

```

## Zadanie 8.5
Za pomocą techniki programowania dynamicznego napisać program obliczający wartości funkcji P(i, j). Porównać z wersją rekurencyjną programu. Wskazówka: Wykorzystać tablicę dwuwymiarową (np. słownik) do przechowywania wartości funkcji. Wartości w tablicy wypełniać kolejno wierszami.

### Python:

```python
def p_dynamic(i:int,j:int):
    p_arq[(0,0)] = 0.5
    for c in range(1,i+1):
        p_arq[(c,0)] = 0.0
    for c in range(1,j+1):
        p_arq[(0,c)] = 1.0
    for c in range(1,i+1):
        for k in range(1,j+1):
            p_arq[(c,k)] = 0.5* (p_arq[(c-1),k]+p_arq[(c,k-1)])
    return p_arq[(i,j)]
```
