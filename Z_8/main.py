from math import gcd,sqrt
from random import uniform

def solve1(a: int,b: int,c: int):
    '''Rozwiazanie rownania liniowego postaci ax + by + c'''
    d = gcd(a,b)
    if c%d != 0 :
        print('Nie ma rozwiązań')
    else:
        aprim = a/d
        bprim = b/d
        print('Równania są postaci (gdzie t są dowolnymi liczbami całkowitymi a x0 i y0 pierwszymi rozwiazaniami):')
        print('x','=','x0'+'+'+str(bprim)+'*t')
        print('y','=','y0'+'-'+str(aprim)+'*t')

def calc_pi(n: int=100):
    r = 10
    p = 2
    msc = 0
    for i in range(0,n):
        x = uniform(-10,10)
        y = uniform(-10,10)
        if x**2+y**2<100:
            msc+=1
    return 4 * (msc/n)

def herona(a: int, b: int, c: int) :
    '''Obliczanie pola powierchni prostokąta za pomocą wzoru herona'''
    if a+b > c and a+c > b and b+c > a:
        p = 0.5 *(a + b + c)
        s = sqrt(p*(p-a)*(p-b)*(p-c))
        return p
    else:
        raise ValueError('Z podanych boków nie da się zbudować trójkąta')
ackerman_tab = {}

#Cos nie gra
def ackerman(n: int, p: int) :
    """Funkcja Ackermanna, n i p to liczby naturalne.
    Zachodzi A(0, p) = 1, A(n, 1) = 2n, A(n, 2) = 2 ** n,
    A(n, 3) = 2 ** A(n-1, 3).
    """
    if (n,p) in ackerman_tab:
        return ackerman_tab[(n,p)]
    else:
        if n == 0:
            r = p+1
        if n > 0 and p == 0:
            r = ackerman(n-1,1)
        if n > 0 and p > 0:
            r = ackerman(n-1,ackerman(n,p-1))
        ackerman_tab[(n,p)] = r
        return r

p_arr = {}
def p(i: int,j: int):
    if (i,j) in p_arr:
        return p_arr[(i,j)]
    else:
        if i == 0 and j == 0:
            r = 0.5
        elif i > 0 and j==0 :
            r = 0.0
        elif j>0 and i==0:
            r = 1.0
        else:
            r = 0.5 * (p(i-1,j) + p(i,j-1))
        p_arr[(i,j)] = r
        return r

p_arq = {}
def p_dynamic(i:int,j:int):
    p_arq[(0,0)] = 0.5
    for c in range(1,i+1):
        p_arq[(c,0)] = 0.0
    for c in range(1,j+1):
        p_arq[(0,c)] = 1.0
    for c in range(1,i+1):
        for k in range(1,j+1):
            p_arq[(c,k)] = 0.5* (p_arq[(c-1),k]+p_arq[(c,k-1)])
    return p_arq[(i,j)]

#Check something
solve1(3,5,-11)
print(calc_pi(10000))
print(herona(3,4,5))
g = ackerman(2,1)
print(g)
#g = p(1010,500)
#print(g)
g = p_dynamic(1010,500)
print(g)
