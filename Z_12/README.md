# Zadania 12
Autor: Michał Skorupa <br>
Data: 26.10.2018 <br>
Programy proszę wykonać w pythonie 3 <br>


## ZADANIE 12.2 (WYSZUKIWANIE BINARNE)
Napisać wersję rekurencyjną wyszukiwania binarnego.

```python
def binarne_rek(L, left, right, y):
    """Wyszukiwanie binarne w wersji rekurencyjnej."""
    pass
```
Problem jest gdy wartosc nie znajduje sie w tablicy, wtedy funkcja ma problem z zbyt duzym zagniezdrzeniem rekurencji

## ZADANIE 12.3 (MEDIANA)
Zaimplementować prostą metodę znajdowania mediany polegającą na posortowaniu zbioru i wybraniu elementu środkowego.

```python
def mediana_sort(L, left, right): pass
```

## ZADANIE 12.6 (LIDER)
Zaimplementować prostą metodę znajdowania lidera w zbiorze nieuporządkowanym przez stworzenie licznika wystąpień za pomocą słownika Pythona.

```python
def lider_py(L, left, right): pass
```
