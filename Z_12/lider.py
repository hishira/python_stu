from operator import itemgetter
def lider(L:list,left:int,right:int):
    count_dict = {}
    for i in range(left,right):
        count_dict[L[i]] = 0
    for i in L[left:right]:
        count_dict[i] += 1

    count_dict = sorted(count_dict,key=count_dict.get)
    return count_dict[-1]

l = [4,4,4,5,5]

print(lider(l,0,len(l)))
