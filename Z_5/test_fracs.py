import unittest
import fracs as frac

class TestFractions(unittest.TestCase):
    '''Klasa do testowania funkcji zawartych w pliku fracs.py'''
    def setUp(self):
        self.fracDict = {
            'frac1':[1,2] ,
            'frac2': [0,1],
            'frac3': [5,6],
            'frac4': [4,5],
            'frac5': [6,8],
            'frac6': [11,16],
        }
        print('Metoda wykonywana na początku każdego testu')

    def tearDown(self):
        print("Metoda wykonywana po kazdym tescie")

    def test_add_frac(self):
        self.assertEqual(frac.add_frac(self.fracDict['frac1'],self.fracDict['frac1']),[1,1])
        self.assertEqual(frac.add_frac(self.fracDict['frac5'],self.fracDict['frac2']),[3,4])
        self.assertEqual(frac.add_frac(self.fracDict['frac4'],self.fracDict['frac5']),[31,20])
    
    def test_sub_frac(self):
        self.assertEqual(frac.sub_frac(self.fracDict['frac4'],self.fracDict['frac3']),[-1,30])
        self.assertEqual(frac.sub_frac(self.fracDict['frac5'],self.fracDict['frac1']),[1,4])
        self.assertEqual(frac.sub_frac(self.fracDict['frac3'],self.fracDict['frac6']),[7,48])
    
    def test_mul_frac(self):
        self.assertEqual(frac.mul_frac(self.fracDict['frac1'],self.fracDict['frac1']),[1,4])
        self.assertEqual(frac.mul_frac(self.fracDict['frac3'],self.fracDict['frac4']),[2,3])
        self.assertEqual(frac.mul_frac(self.fracDict['frac5'],self.fracDict['frac6']),[33,64])
    
    def test_div_frac(self):
        self.assertEqual(frac.div_frac(self.fracDict['frac1'],self.fracDict['frac1']),[1,1])
        self.assertEqual(frac.div_frac(self.fracDict['frac5'],self.fracDict['frac6']),[12,11])
        self.assertEqual(frac.div_frac(self.fracDict['frac3'],self.fracDict['frac4']),[25,24])
    
    def test_is_positive(self):
        self.assertEqual(frac.is_positive(self.fracDict['frac4']),True)
        self.assertEqual(frac.is_positive([4,-5]),False)
        self.assertEqual(frac.is_positive([3,4]),True)
    
    def test_is_zero(self):
        self.assertEqual(frac.is_zero(self.fracDict['frac2']),True)
        self.assertEqual(frac.is_zero([4,8]),False)
        self.assertEqual(frac.is_zero([7,45]),False)
     
    def test_cmp_frac(self):
        self.assertEqual(frac.cmp_frac(self.fracDict['frac3'],self.fracDict['frac5']),1)
        self.assertEqual(frac.cmp_frac([4,6],[5,6]),-1)
        self.assertEqual(frac.cmp_frac([4,45],[4,45]),0)
    
    def test_frac2float(self):
        self.assertEqual(frac.frac2float(self.fracDict['frac5']),0.75)
        self.assertEqual(frac.frac2float([1,2]),0.5)
        self.assertEqual(frac.frac2float([4,5]),0.8)
        
        
if __name__ == '__main__':
    unittest.main()
    