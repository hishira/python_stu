    
def factorial(n):
    result = 1
    while n > 0:
        result*= n
        n-=1
    return result

def fibonaci(n):
    if n == 1 or n ==2:
        return 1
    a,b = 1, 1
    i = 2
    while i < n:
        c = a + b
        b = a
        a = c
        i += 1
    return a
    