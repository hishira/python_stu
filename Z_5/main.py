import rekurencja
print('import rekurencja')
print(rekurencja.factorial(6))
print(rekurencja.fibonaci(5))
del rekurencja
print()
import rekurencja as rek
print('import rekurencja as rek')
print(rek.factorial(6))
print(rek.fibonaci(5))
del rek
print()
from rekurencja import *
print('from rekurencja import *')
print(factorial(6))
print(fibonaci(5))
del factorial
del fibonaci
print()
from rekurencja import factorial
from rekurencja import fibonaci as fib
print('from rekurencja import factorial')
print('from rekurencja import fibonaci as fib')
print(factorial(6))
print(fib(5))
del factorial
del fib
