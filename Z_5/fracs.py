import math

def add_frac(frac1,frac2):
    if frac1[1] == frac2[1]:
        licznik = frac1[0] + frac2[0] 
        nwd = math.gcd(licznik, frac1[1])
        return [licznik / nwd, frac1[1]/nwd]
    else:
        nww = ( frac1[1] * frac2[1] ) / math.gcd(frac1[1],frac2[1])
        licznik = ( frac1[0] * ( nww / frac1[1] ) ) + ( frac2[0] * (nww / frac2[1] ) )
        nwd = math.gcd(int(licznik),int(nww))
        return [licznik  / nwd , nww / nwd ]

def sub_frac(frac1,frac2):
    if frac1[1] == frac2[1]:
        licznik = frac1[0] - frac2[0]
        nwd = math.gcd(licznik,frac1[1])
        return [licznik / nwd, frac1[1] / nwd]
    else:
        nww = (frac1[1] * frac2[1]) / math.gcd(frac1[1],frac2[1])
        licznik = (frac1[0] * (nww / frac1[1] ) ) - ( frac2[0] * ( nww / frac2[1] ) )
        nwd = math.gcd(int(licznik),int(nww))
        return [licznik / nwd, nww / nwd ] 
        
def mul_frac(frac1,frac2):
    licznik = frac1[0] * frac2[0]
    mianownik = frac1[1] * frac2[1]
    nwd = math.gcd(licznik,mianownik)
    return [licznik / nwd, mianownik / nwd]

def div_frac(frac1,frac2):
    return mul_frac(frac1,[frac2[1],frac2[0]])

def is_positive(frac):
    return True if (frac[0] > 0 and frac[1] > 0) or (frac[0] < 0 and frac[1] < 0) else False

def cmp(f,g):
    if f < g :
        return -1
    elif f == g:
        return 0
    else:
        return 1

def cmp_frac(frac1,frac2):
    if frac1[1] == frac2[1]:
        return cmp(frac1[0],frac2[0])
    else:
        nww = (frac1[1] * frac2[1]) / math.gcd(frac1[1],frac2[1])
        frac1_licznik = frac1[0] * (nww / frac1[1] )
        frac2_licznik = frac2[0] * (nww / frac2[1])
        return cmp(frac1_licznik,frac2_licznik)
        
def is_zero(frac):
    return True if frac[0] == 0 else False

def frac2float(frac):
    return float(frac[0]/frac[1])
