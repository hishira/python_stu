def add_poly(poly1,poly2):
    if len(poly1) == len(poly2):
        return [x + poly2.pop(0) for x in poly1]
    else:
        maximum = max(len(poly1),len(poly2))
        poly1.extend([ 0 for i in range(len(poly1),maximum)])
        poly2.extend([0 for i in range(len(poly2),maximum)])
        return [x + poly2.pop(0) for x in poly1]

def sub_poly(poly1,poly2):
    if len(poly1) == poly2:
        return [ x - poly2.pop(0) for x in poly1]
    else:
        maximum = max(len(poly1),len(poly2))
        poly1.extend([0 for i in range(len(poly1),maximum)])
        poly2.extend([0 for i in range(len(poly2),maximum)])
        return [ x - poly2.pop(0) for x in poly1]

def mul_poly(p1,p2):
    maximum = max(len(p1), len(p2))
    p1.extend([0 for i in range(len(p1),maximum)])
    p2.extend([0 for i in range(len(p2),maximum)])
    return [ p1[0]*p2[0], p1[0]*p2[1]+p1[1]*p2[0] ,p1[0]*p2[2]+p1[1]*p2[1]+p1[2]*p2[0] ,p1[1]*p2[2]+p1[2]*p2[1],p1[2]*p2[2] ]  

def is_zero(poly):
    return not any(poly)

def cmp_poly(p1,p2):
    if len(p1) == len(p2):
        if p1 == p2:
            return 0
        if p1[-1] > p2[-1]:
            return 1
        else:
            return -1
    else:
        maximum = max(len(p1), len(p2))
        p1.extend([0 for i in range(len(p1),maximum)])
        p2.extend([0 for i in range(len(p2),maximum)])

#Dokończyć
def  eval_poly(poly,x0):
    result = 0
    for i,k in enumerate(poly):
        result += k* (x0**i)
    return result

#print(add_poly([1,1,1],[1,1]))
#print(sub_poly([1],[1,-1,1]))
print(mul_poly([1,0,-1],[1,0,1]))
print(is_zero([1]))
print(eval_poly([1,2,3],4))