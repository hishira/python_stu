# Zadanie 5
Autor: Michal Skorupa
Data: 28.10.2018
Programy prosze uruchamiac przy pomocy python 3

- Zadanie 5.1
Zadanie polegalo na sprawdzeniu operacji importu jak i przeladowania modulu
Glowny program zapisany jest w pliku main.py, funkcje factorial() i fibonaci()
znajduja sie w module rekurencja.py. Program za pomaca wywolan print informuje
w jaki sposob wywolania import byly stosowane. Argumentu funkcji sa takie same
dla factorial 6 a dla fibonaci 5 ale funkcje te przy kazdym wywolaniu sa
inaczej importowane.

- Zadanie 5.2
W pliku fracs.py zostaly zaimplementowane wszystkie funkcje jakie byly podane
w zadaniu. W module test_fracs.py zotaly zaimplementowane testy jednostkowe
dla funkcji zawartych w module fracs.py
