from binarysearchtree import Node


def count_leafs(top:Node) -> int:
    '''Funckaj ma za zadanie zliczyc liczbe wierzcholkow
    w drzewie ktore nie maja potomkow wersja iteracyjna
    @param top: wierzcholek, root drzewa
    @return int: liczba wszystkich lisci w drzewie
    '''
    if top.data == None:
        raise ValueError('Lista jest pusta')

    stos = []
    counter = 0
    stos.append(top)
    while len(stos)!=0:
        vertex = stos.pop()
        if vertex.left:
            stos.append(vertex.left)
        if vertex.right:
            stos.append(vertex.right)
        elif vertex.left == None and vertex.right == None:
            counter+=1
    return counter

def count_total(top:Node) -> int :
    '''
    Jesli w weirzcholkach przechowywane sa liczby to funkcja zlicza
    sume wszystkich liczb w tych wierzcholkach
    @param top: korzen drzewa
    @return int: suma wszystkich liczb w drzewie
    '''
    suma = top.data
    if top.left:
        suma += count_total(top.left)
    if top.right:
        suma += count_total(top.right)
    return suma


if __name__ == '__main__':
    head = Node(20)
    head.insert(15)
    head.insert(25)
    head.insert(6)
    head.insert(17)
    head.insert(24)
    head.insert(35)
    print(count_leafs(head))
    print(head.search(35))
    print(count_total(head))
