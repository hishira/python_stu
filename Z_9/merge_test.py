import unittest
from sys import exit
from merge import merge
from Node import Node

filename = 'test_1.txt'
test = False
testy = {
        'test1': [],
        'test2': [],
        'test3': [],
        }
try:
    with open(filename) as file_obj:
        file_lines = file_obj.readlines()
except FileNotFoundError:
    print('Pliku nie znaleziono')

else:
    for line in file_lines:
        line = line.replace('\n','')
        line = line.strip()
        if 'test' in line:
            test = True
            testName = line
        elif test:
            number = line.split()
            if line!= '' and  line[0].isnumeric():
                testy[testName].append(list(int(x) for x in number))


def matchListToArray(node,arr:list):
    tmp = node
    i = 0
    while tmp:
        if tmp.data != arr[i]:
            return False
        tmp = tmp.next
        i+=1
    if i != len(arr):
        return False
    return True

class TestMerge(unittest.TestCase):
    def setUp(self):
        #Test1
        self.node1,self.node2,self.lista = None,None,[]
        for element in testy['test1'][0]:
            self.node1 = Node(element,self.node1)
        for element in testy['test1'][1]:
            self.node2 = Node(element,self.node2)
        self.lista = testy['test1'][2].copy()

        #Test2
        self.node3,self.node4,self.lista2 = None,None,[]
        for element in testy['test2'][0]:
            self.node3 = Node(element,self.node3)
        for element in testy['test2'][1]:
            self.node4 = Node(element,self.node4)
        self.lista2 = list(reversed(testy['test2'][0]))+list(reversed(testy['test2'][1]))

        #Test3
        self.node5,self.node6,self.lista3 = None,None,[]
        for element in testy['test3'][0]:
            self.node5 = Node(element,self.node5)
        for element in testy['test3'][1]:
            self.node6 = Node(element,self.node6)
        self.lista3 = list(reversed(testy['test3'][0]))+list(reversed(testy['test3'][1]))

    def testMerge(self):
        self.assertTrue(matchListToArray(merge(self.node1,self.node2),self.lista))
        self.assertTrue(matchListToArray(merge(self.node3,self.node4),self.lista2))
        self.assertTrue(matchListToArray(merge(self.node5,self.node6),self.lista3))

if __name__ == '__main__':
    unittest.main()
