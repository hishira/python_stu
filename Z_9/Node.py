
class Node():
    def __init__(self,data:int=None,next=None):
        self.data = data
        self.next = next
    def __str__(self):
        return str(self.data)

        
def traverse(head:Node):
    tmp = head
    while tmp!=None:
        print(tmp.data, end=', ')
        tmp = tmp.next
    
if __name__ == '__main__':
    head = None
    head = Node(3,head)
    head = Node(4,head)
    traverse(head)