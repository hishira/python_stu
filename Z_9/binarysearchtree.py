class Node():
    '''Klasa rreprezentujaca jednen wezel'''

    def __init__(self,data=None,left=None,right=None):
        '''Constructor'''
        self.data = data
        self.left = left
        self.right = right

    def __str__(self) -> str:
        '''String reprezentation of Node'''
        return str(self.data)

    def insert(self,data):
        '''Wstawia podana wasrtosc do drzewa binarnego
        @param data: wstawia podana wartosc do drzewa
        '''
        if self.data < data:
            if self.right:
                self.right.insert(data)
            else:
                self.right = Node(data)
        elif self.data > data:
            if self.left:
                self.left.insert(data)
            else:
                self.left = Node(data)
        else:
            pass

    def search(self, data) -> bool:
        '''szukamy odpowiedniej wartosco
        @return True: jezeli wartosc znajduje sie w drzewie
        @return False: jezeli wartosc nie znajduje sie w drewie
        '''
        if self.data == data:
            return True
        elif self.data > data:
            if self.left:
                return self.left.search(data)
        elif self.data < data:
            if self.right:
                return self.right.search(data)

        return False

def count(head) ->int:
    counter = 1
    if head:
        if head.left:
            counter += count(head.left)
        if head.right:
            counter += count(head.right)
    return counter

class SearchBinaryTree():
    '''Klasa prezentujaca binarne drzewo przeszukiwan'''
    def __init__(self):
        '''Countruction for BTS'''
        self.root = None

    def insert(self,data):
        '''Wstawianie wartosci do drzewa
        @param data: wartosc wsadzana do drzewa'''
        if self.root:
            self.root.insert(data)
        else:
            self.root = Node(data)

    def search(self,data) ->bool:
        '''Funkcja szuka czy podana wartosc znajduje sie w
        drzewie BTS
        @param data: wartosc szukana
        @return bool: wartosc True albo False w zaleznosci czy istnieje
        '''
        return self.root.search(data) if self.root else False

if __name__ == '__main__':
    root = SearchBinaryTree()
    root.insert(20)
    root.insert(34)
    root.insert(45)
    root.insert(30)
    root.insert(456)
    print(count(root.root))
    print(root.search(45))
