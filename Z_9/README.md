# Zadania 9
Autor: Michał Skorupa 16.10.2018
Wykonane zadania: 9.2, 9.7, 9.8

## ZADANIE 9.2 (SINGLELIST)
Mamy listy jednokierunkowe bez klasy SingleList. Napisać funkcję merge(), która łączy dwie listy przez podłączenie drugiej na koniec pierwszej, a zwraca nowy początek wspólnej listy. Uwzględnić możliwość pustych list.

```python
def merge(node1, node2): pass

# Zastosowanie.
head3 = merge(head1, head2)
# Czyszczenie łączy.
del head1
del head2
```
Dodatkowo zostały wykonane proste testy

## ZADANIE 9.7 (BINARYTREE)
Mamy drzewo binarne bez klasy BinarySearchTree. Napisać funkcję count_leafs(top), która liczy liście drzewa binarnego. Liście to węzły, które nie mają potomków. Można podać rozwiązanie rekurencyjne lub rozwiązanie iteracyjne, które jawnie korzysta ze stosu.

Załóżmy, że drzewo binarne przechowuje liczby. Napisać funkcję calc_total(top), która podaje sumę liczb przechowywanych w drzewie.

```python
def count_leafs(top): pass

def count_total(top): pass
```

## ZADANIE 9.8 (BINARYSEARCHTREE)
Dla drzewa BST napisać funkcje znajdujące największy i najmniejszy element przechowywany w drzewie. Mamy łącze do korzenia, nie ma klasy BinarySearchTree. Drzewo BST nie jest modyfikowane, a zwracana jest znaleziona wartość (węzeł). W przypadku pustego drzewa należy wyzwolić wyjątek ValueError.

```python
def bst_max(top): pass

def bst_min(top): pass
