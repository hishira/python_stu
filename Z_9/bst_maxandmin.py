from binarysearchtree import Node

def bst_max(top:Node) -> int:
    '''Funkcja do szukania najwiekszej wartosc w drzewie BST
    @param top: Node, czyli korzen drzewa
    '''
    if top.data == None:
        raise ValueError('Drzewo jest puste')
    tmp = top
    while tmp.right:
        tmp = tmp.right
    return tmp.data

def bst_min(top:Node) -> int:
    '''Funkcja zwraca najmniejsza wartosc w drzewie
    @param top: korzen drzewa
    '''
    if top.data == None:
        raise valueError('Drzewo jest puste')
    tmp = top
    while tmp.left:
        tmp = tmp.left
    return tmp.data

if __name__ == '__main__':
    head = Node(34)
    head.insert(12)
    head.insert(3)
    head.insert(3221)
    head.insert(32)
    head.insert(432)
    print(bst_max(head))
    print(bst_min(head))
