import unittest
from rectangles import Rectangle
from points import Point
class TestRectangle(unittest.TestCase):
    '''Klasa testujaca klase Rectangle'''
    
    def setUp(self):
        self.rectangleDict = {
                'Rectangle1':Rectangle(2,1,6,4),
                'Rectangle2':Rectangle(2,1,4,5),
                'Rectangle3':Rectangle(-1,1,3,2),
                'Rectangle4':Rectangle(0,0,7,5),
                'Rectangle5':Rectangle(0,-4,5,0),
        }
    
    def test_strRectangle(self):
        self.assertEqual(str(self.rectangleDict['Rectangle1']),'[(2, 1), (6, 4)]')
        self.assertEqual(str(self.rectangleDict['Rectangle3']),'[(-1, 1), (3, 2)]')
        self.assertEqual(str(self.rectangleDict['Rectangle5']),'[(0, -4), (5, 0)]')

    def test_reprRectangle(self):
        self.assertEqual(repr(self.rectangleDict['Rectangle2']),'Rectangle(2, 1, 4, 5)')
        self.assertEqual(repr(self.rectangleDict['Rectangle3']),'Rectangle(-1, 1, 3, 2)')
        self.assertEqual(repr(self.rectangleDict['Rectangle4']),'Rectangle(0, 0, 7, 5)')
    
    def test_eqRectangel(self):
        self.assertEqual(self.rectangleDict['Rectangle3'] == self.rectangleDict['Rectangle3'] ,True)
        self.assertEqual(self.rectangleDict['Rectangle2'] == self.rectangleDict['Rectangle3'] ,False)
        self.assertEqual(Rectangle(4,4,5,5) == Rectangle(4,4,5,5) ,True)
     
    def test_nqRectangle(self):
        self.assertEqual(self.rectangleDict['Rectangle3'] != self.rectangleDict['Rectangle4'],True)
        self.assertEqual(self.rectangleDict['Rectangle3'] != self.rectangleDict['Rectangle3'],False)
        self.assertEqual(Rectangle(45,50,48,49) != Rectangle(903,345,544,545),True)
    
    def test_centerRectangle(self):
        self.assertEqual(self.rectangleDict['Rectangle3'].center(), Point(1,1.5))
        self.assertEqual(self.rectangleDict['Rectangle4'].center(), Point(3.5,2.5))
        self.assertEqual(Rectangle(25,50,525,850).center(), Point(275,450))
    
    def test_areaRectangel(self):
        self.assertEqual(self.rectangleDict['Rectangle3'].area(),4)
        self.assertEqual(self.rectangleDict['Rectangle5'].area(),20)
        self.assertEqual(Rectangle(4,4,8,6).area(),8)
    
    def test_moveRectangle(self):
        self.rectangleDict['Rectangle4'].move(5,5)
        self.rectangleDict['Rectangle5'].move(50,45)
        rectangle = Rectangle(45,50,675,985)
        rectangle.move(15,15)
        self.assertEqual(self.rectangleDict['Rectangle4'],Rectangle(5,5,12,10))
        self.assertEqual(self.rectangleDict['Rectangle5'],Rectangle(50,41,55,45))
        self.assertEqual(rectangle,Rectangle(60,65,690,1000))
        
if __name__ == '__main__':
    unittest.main()