import unittest
from points import Point
#783789532 filek sławomir

class PointTest(unittest.TestCase):
    '''Testowanie klasy point'''
    
    def setUp(self):
        self.pointDict = {
                'Point1':  Point(1,1),
                'Point2': Point(5,4),
                'Point3': Point(10,20),
                'Point4': Point(15,-4),
                'Point5': Point(-6,7)
        }
    
    def test_strPoint(self):
        self.assertEqual(str(self.pointDict['Point1']),'(1, 1)')
        self.assertEqual(str(self.pointDict['Point4']),'(15, -4)')
        self.assertEqual(str(self.pointDict['Point5']),'(-6, 7)')
    
    def test_reprPoint(self):
        self.assertEqual(repr(self.pointDict['Point2']),'Point(5, 4)')
        self.assertEqual(repr(self.pointDict['Point3']),'Point(10, 20)')
        self.assertEqual(repr(self.pointDict['Point5']),'Point(-6, 7)')
        
    def test_eqPoint(self):
        self.assertEqual(self.pointDict['Point1']== Point(1,1),True)
        self.assertEqual(self.pointDict['Point5']== Point(60,70),False)
        self.assertEqual(self.pointDict['Point4']== self.pointDict['Point4'],True)
    
    def test_nePoint(self):
        self.assertEqual(self.pointDict['Point5'] !=Point(-6,7) ,False)
        self.assertEqual(self.pointDict['Point5'] !=self.pointDict['Point4'] ,True)
        self.assertEqual(self.pointDict['Point1'] !=Point(40,40) ,True)

    def test_addPoint(self):
        self.assertEqual(self.pointDict['Point4']+self.pointDict['Point4'],Point(30,-8))
        self.assertEqual(self.pointDict['Point4']+self.pointDict['Point3'],Point(25,16))
        self.assertEqual(Point(5,4) + Point(45,44),Point(50,48))
    
    def test_subPoint(self):
        self.assertEqual(self.pointDict['Point4'] - self.pointDict['Point3'] , Point(5,-24))
        self.assertEqual(self.pointDict['Point5'] - Point(44,45) , Point(-50,-38))
        self.assertEqual(Point(5,4) - Point(-4,-5) , Point(9,9))
     
    def test_mulPoint(self):
        self.assertEqual(self.pointDict['Point4'] * self.pointDict['Point3'], 70)
        self.assertEqual(self.pointDict['Point5'] * Point(3,4), 10)
        self.assertEqual(Point(-8,4) * Point(10,15),-20 )
    
    def test_crossPoint(self):
        self.assertEqual(self.pointDict['Point4'].cross(self.pointDict['Point5']),81)
        self.assertEqual(self.pointDict['Point4'].cross(Point(10,-45)),-635)
        self.assertEqual(Point(40,30).cross(Point(4,5)),80)
    def test_lengthPoint(self):
        self.assertEqual(self.pointDict['Point4'].length(), 15.524174696260024)
        self.assertEqual(self.pointDict['Point3'].length(), 22.360679774997898)
        self.assertEqual(Point(3,4).length(),5)
    
if __name__ =='__main__':
    unittest.main()