from points import Point

class Rectangle():
    '''Klasa reprezentująca prostokąt na plaszczyźnie
        poprzez dwa punkty , lewy dolny i prawy górny'''

    def __init__(self,x1,y1,x2,y2):
        try:
            if x1<= x2 and y1<=y2:
                self.pt1 = Point(x1,y1)
                self.pt2 = Point(x2,y2)
            else:
                raise ValueError
        except ValueError:
            #Obsluga wyjatku ValueError
            print('Błędne dane, nie mozna utworzyć obiektu,zgłoszenie wyjatku')

            raise ValueError
    def __str__(self):
        return '[({self.pt1.x}, {self.pt1.y}), ({self.pt2.x}, {self.pt2.y})]'.format(self=self)

    def __repr__(self):
        return 'Rectangle({self.pt1.x}, {self.pt1.y}, {self.pt2.x}, {self.pt2.y})'.format(self=self)

    def __eq__(self,other):
        return True if self.pt1 == other.pt1 and self.pt2 == other.pt2 else False

    def __ne__(self,other):
        return not self.__eq__(other)

    def center(self):
        return Point((self.pt1.x+self.pt2.x)/2,(self.pt1.y+self.pt2.y)/2)

    def area(self):
        c = Point(self.pt2.x,self.pt1.y)
        a = c - self.pt1
        b = self.pt2 - c
        return a.length() * b.length()

    def move(self,x,y):
        self.pt1.x += x
        self.pt1.y += y
        self.pt2.x += x
        self.pt2.y += y

    def intersection(self,other):
        '''Zwracamy pole częsci wspólnej protstokatow'''
        flag = True if other.pt1.x >= self.pt1.x and other.pt1.x <= self.pt2.x else False
        if flag:
            if other.pt2.x >= self.pt2.x:
                return Rectangle(other.pt1.x,other.pt1.y,self.pt2.x,self.pt2.y)
            elif other.pt2.x < self.pt2.x and (other.pt2.y > self.pt2.y and other.pt2.y < self.pt2.y):
                return other
            elif other.pt2.x < self.pt2.x:
                return Rectangle(other.pt1.x,other.pt1.y,other.pt1.x,self.pt2.y)
        elif (other.pt1.y >= self.pt1.y and other.pt1.y <= self.pt2.y) and not (flag):
            if other.pt2.x < self.pt2.x:
                return Rectangle(self.pt1.x,other.pt1.y,other.pt2.x,self.pt2.y)
        else:
            raise ValueError('Prostokąty nie pokrywają sie')

    def cover(self,other):
        minimumX = min(self.pt1.x, other.pt1.x)
        minimumY = min(self.pt1.y, other.pt1.y)
        maximumX = max(self.pt2.x,other.pt2.x)
        maximumY = max(self.pt2.y,other.pt2.y)
        return Rectangle(minimumX,minimumY,maximumX,maximumY)

    def make4(self):
        center = self.center()
        pt3 = Point(self.pt1.x,self.pt2.y)
        pt4 = Point(self.pt2.x,self.pt1.y)
        A = Point(float((self.pt1.x+pt3.x)/2),float((self.pt1.y+pt3.y)/2))
        B = Point(float((pt3.x+self.pt2.x)/2),float((pt3.y+self.pt2.y)/2))
        C = Point(float((self.pt2.x+pt4.x)/2),float((self.pt2.y+pt4.y)/2))
        D = Point(float((self.pt1.x+pt4.x)/2),float((self.pt1.y+pt4.y)/2))
        return [Rectangle(A.x,A.y,B.x,B.y),Rectangle(center.x,center.y,self.pt2.x,self.pt2.y),Rectangle(self.pt1.x,self.pt1.y,center.x,center.y),Rectangle(D.x,D.y,C.x,C.y)]
