from circles import Circle

import unittest

class testCircle(unittest.TestCase):
    def setUp(self):
        self.circlesDict = {
                 'Circle1': Circle(-5,-6,40),
                 'Circle2': Circle(4,5,10),
                 'Circle3': Circle(3,5,40),
                 'Circle4': Circle(3,4,20),
                 'Circle5': Circle(6,7,8),
            }
            
    def test_reprCircleAndError(self):
        self.assertEqual(repr(self.circlesDict['Circle1']),'Circle(-5, -6, 40)')
        self.assertEqual(repr(self.circlesDict['Circle5']),'Circle(6, 7, 8)')
        self.assertEqual(repr(Circle(3,10,20)),'Circle(3, 10, 20)')
        with self.assertRaises(ValueError):
            Circle(3,4,-30)
    
    def test_eqCircle(self):
        self.assertEqual(self.circlesDict['Circle3'] == self.circlesDict['Circle5'],False)
        self.assertEqual(self.circlesDict['Circle4']==Circle(3,4,20),True)
        self.assertEqual(Circle(4,5,950) == Circle(4,5,950), True)
    
    def test_nqCircle(self):
        self.assertEqual(self.circlesDict['Circle3'] != self.circlesDict['Circle5'],True)
        self.assertEqual(self.circlesDict['Circle3'] != Circle(3,5,40),False)
        self.assertEqual(Circle(45,55,543) != Circle(534,345,654) , True)
    
    def test_areaCircle(self):
        self.assertEqual(self.circlesDict['Circle3'].area(),5026.548245743669)
        self.assertEqual(self.circlesDict['Circle5'].area(),201.06192982974676)
        self.assertEqual(Circle(6,7,45).area(),6361.725123519331)
        
    def test_moveCircle(self):
        circle = Circle(45,55,60)
        circle_ver = Circle(43,532,6412)
        circle_ver_ok = Circle(45,553,60)
        circle.move(10,20)
        circle_ver.move(34,43)
        circle_ver_ok.move(54,34)
        self.assertEqual(circle == Circle(55,75,60), True)
        self.assertEqual(circle_ver == Circle(77,575,6412), True)
        self.assertEqual(circle_ver_ok == Circle(99,587,60), True)
        
    def test_coverCircle(self):
        self.assertEqual(self.circlesDict['Circle3'].cover(self.circlesDict['Circle5']),self.circlesDict['Circle3'])
        self.assertEqual(self.circlesDict['Circle3'].cover(Circle(1,1,10)),self.circlesDict['Circle3'])
        self.assertEqual(Circle(4,5,5).cover(Circle(10,20,6)),Circle(4,5,22.15549442140351))
        
if __name__ == '__main__':
    unittest.main()