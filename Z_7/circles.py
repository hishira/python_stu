from points import Point
from math import pi,sqrt

class Circle():
    '''Klasa reprezentujaca okregi na plaszczyznie'''
    
    def __init__(self, x ,y , radius):
        if radius < 0:
            raise ValueError('Promien ujemny')
        self.pt = Point(x, y)
        self.radius = radius
        
    def __repr__(self):
        return 'Circle({self.pt.x}, {self.pt.y}, {self.radius})'.format(self=self)
    
    def __eq__(self,other):
        return self.pt == other.pt and self.radius == other.radius
    
    def __ne__(self,other):
        return not self == other
    
    def area(self):
        return pi * self.radius**2
        
    def move(self,x,y):
        self.pt.x += x
        self.pt.y += y
    
    def cover(self,other):
        arr = [other.pt.x-self.pt.x,other.pt.y-self.pt.y]
        length = sqrt(arr[0]**2 + arr[1]**2)
        if length < self.radius:
            return self
        else:
            return Circle(self.pt.x,self.pt.y, length+other.radius)