import unittest

from rectangles import Rectangle
from points import Point

class TestRectangle(unittest.TestCase):
    '''Klasa testująca klase Rectangle z wyjątkami'''

    def setUp(self):
        self.rectangleDict = {
                'Rectangle1':Rectangle(2,1,6,4),
                'Rectangle2':Rectangle(2,1,4,5),
                'Rectangle3':Rectangle(-1,1,3,2),
                'Rectangle4':Rectangle(0,0,7,5),
                'Rectangle5':Rectangle(0,-4,5,0),
        }

    def test_initRaise(self):
        with self.assertRaises(ValueError) as error:
            rec = Rectangle(34,32,3,423)
            rec_ver = Rectangle(10,20,321,5)

    def test_strRectangle(self):
        self.assertEqual(str(self.rectangleDict['Rectangle1']),'[(2, 1), (6, 4)]')
        self.assertEqual(str(self.rectangleDict['Rectangle3']),'[(-1, 1), (3, 2)]')
        self.assertEqual(str(self.rectangleDict['Rectangle5']),'[(0, -4), (5, 0)]')

    def test_reprRectangle(self):
        self.assertEqual(repr(self.rectangleDict['Rectangle2']),'Rectangle(2, 1, 4, 5)')
        self.assertEqual(repr(self.rectangleDict['Rectangle3']),'Rectangle(-1, 1, 3, 2)')
        self.assertEqual(repr(self.rectangleDict['Rectangle4']),'Rectangle(0, 0, 7, 5)')

    def test_eqRectangel(self):
        self.assertEqual(self.rectangleDict['Rectangle3'] == self.rectangleDict['Rectangle3'] ,True)
        self.assertEqual(self.rectangleDict['Rectangle2'] == self.rectangleDict['Rectangle3'] ,False)
        self.assertEqual(Rectangle(4,4,5,5) == Rectangle(4,4,5,5) ,True)

    def test_nqRectangle(self):
        self.assertEqual(self.rectangleDict['Rectangle3'] != self.rectangleDict['Rectangle4'],True)
        self.assertEqual(self.rectangleDict['Rectangle3'] != self.rectangleDict['Rectangle3'],False)
        self.assertEqual(Rectangle(45,50,48,543) != Rectangle(903,345,5454,5455),True)

    def test_centerRectangle(self):
        self.assertEqual(self.rectangleDict['Rectangle3'].center(), Point(1,1.5))
        self.assertEqual(self.rectangleDict['Rectangle4'].center(), Point(3.5,2.5))
        self.assertEqual(Rectangle(25,50,525,850).center(), Point(275,450))

    def test_intersectionRectangle(self):
        self.assertEqual(Rectangle(-3,-5,1,-2).intersection(Rectangle(-1,-4,2,-1)).area(),4)
        self.assertEqual(Rectangle(-3,-5,1,-2).intersection(Rectangle(-2,-4,1,-2)).area(),6)
        self.assertEqual(Rectangle(-3,-5,1,-2).intersection(Rectangle(-4,-4,0,-1)).area(),6)
        with self.assertRaises(ValueError):
            Rectangle(-3,-5,1,-2).intersection(Rectangle(-4,-1,0,3))

    def test_coverRectangle(self):
        self.assertEqual(Rectangle(-3,-5,1,-2).cover(Rectangle(-2,-4,0,-3)),Rectangle(-3,-5,1,-2))
        self.assertEqual(Rectangle(-3,-5,1,-2).cover(Rectangle(-4,-6,2,-1)),Rectangle(-4,-6,2,-1))
        self.assertEqual(Rectangle(-3,-5,1,-2).cover(Rectangle(-2,-4,2,-1)),Rectangle(-3,-5,2,-1))

    def test_make4(self):
        self.assertIn(Rectangle(-4,-6,-1,-3.5),Rectangle(-4,-6,2,-1).make4())
        self.assertIn(Rectangle(-2,-3,0,0),Rectangle(-4,-6,0,0).make4())
        self.assertIn(Rectangle(-3,-6,-2,-3),Rectangle(-4,-6,-2,0).make4())

if __name__ == '__main__':
    unittest.main()
