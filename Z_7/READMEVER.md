# Zestaw 7
Michał Skorupa
19.11.2018
Zadania: 7.3, 7.5

# 7.3
W plku circles.py znajduje sie zaimplementowana klasa Circle, natomiast w pliku
test_circle.py jej moduł testujący.

# 7.5
Implementacja klasy Rectangle znajduje sie w pliku rectangles.py. Modul testujacy
ta klase znajduje sie w test_rectangle.py.
