from math import sqrt

class Point():
    ''' Klasa reprezentujaca punkty na plaszczyznie'''
    def __init__(self,x,y):
        self.x = x
        self.y = y
        
    def __str__(self):
        return '({}, {})'.format(self.x,self.y)
    def __repr__(self):
        return 'Point({self.x}, {self.y})'.format(self=self)
    
    def __eq__(self,other):
        return True if self.x == other.x and self.y ==other.y else False
    
    def __ne__(self,other):
        return not self.__eq__(other)
    
    def __add__(self,other):
        return Point(self.x+other.x,self.y+other.y)
    
    def __sub__(self,other):
        return Point(self.x-other.x, self.y-other.y)
    
    def __mul__(self,other):
        return (self.x*other.x + self.y*other.y)
        
    def cross(self,other):
        return self.x * other.y - self.y * other.x
    
    def length(self):
        return sqrt(self.x**2 + self.y**2)
#print('{:b}'.format(120))