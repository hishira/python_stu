import unittest

from functions import factorial,fibonaci,sum_sequence,odwracanieRekurencja,flatten


class Test(unittest.TestCase):
    '''Test class for factorial and fibonaci functions
            example from:
            https://en.wikipedia.org/wiki/Factorial,
            http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibtable.html
    '''
    def setUp(self):
        self.l1 = [42,7,4,6,7,9,8,5,6,7,0,9,8,7,6,5,4,3]
        self.l2 = [6,5,4,3,7,8,9,0,7,6,5,8,9,7,6,4,7,42]
        self.l3 = [3,4,5,6,7,8,9,0,7,6,5,8,9,7,42,7,4,6]
        self.l4 = [3,4,5,6,7,8,9,8,5,6,7,0,9,7,6,4,7,42]
        odwracanieRekurencja(self.l1,0,17)
        odwracanieRekurencja(self.l2,0,3)
        odwracanieRekurencja(self.l3,14,17)
        odwracanieRekurencja(self.l4,7,11)

    def testFactiorial(self):
        self.assertEqual(factorial(0),1)
        self.assertEqual(factorial(1),1)
        self.assertEqual(factorial(4),24)
        self.assertEqual(factorial(11),39916800)
        self.assertEqual(factorial(14),87178291200)
        self.assertEqual(factorial(14),87178291200)
        self.assertEqual(factorial(19),121645100408832000)
        self.assertEqual(factorial(25),15511210043330985984000000)
        self.assertEqual(factorial(25),15511210043330985984000000)
        self.assertEqual(factorial(35),10333147966386144929666651337523200000000)
        self.assertEqual(factorial(45),119622220865480194561963161495657715064383733760000000000)

    def testFibonaci(self):
        self.assertEqual(fibonaci(10),55)
        self.assertEqual(fibonaci(21),10946)
        self.assertEqual(fibonaci(30),832040)
        self.assertEqual(fibonaci(38),39088169)
        self.assertEqual(fibonaci(44),701408733)
        self.assertEqual(fibonaci(50),12586269025)

    def testSumSequence(self):
        self.assertEqual(sum_sequence([1,1,[1,1,[1,[6,5],1],1]]),18)
        self.assertEqual(sum_sequence([5,6,3,4]),18)
        self.assertEqual(sum_sequence([[1,[45]],[65],[]]),111)
        self.assertEqual(sum_sequence([5,5,[4,5,[5,6,7]]]),37)
        self.assertEqual(sum_sequence([5,4,[[5,4,[]],[5,3],[65]]]),91)

    def testOdwracanie(self):
        self.assertEqual(self.l1, [3,4,5,6,7,8,9,0,7,6,5,8,9,7,6,4,7,42])
        self.assertEqual(self.l2, [3,4,5,6,7,8,9,0,7,6,5,8,9,7,6,4,7,42])
        self.assertEqual(self.l3, [3,4,5,6,7,8,9,0,7,6,5,8,9,7,6,4,7,42])
        self.assertEqual(self.l4, [3,4,5,6,7,8,9,0,7,6,5,8,9,7,6,4,7,42])

    def testFlatten(self):
        self.assertEqual(flatten([1,(2,3),[],[4,(5,6,7)],8,[9]]), [1,2,3,4,5,6,7,8,9])
        self.assertEqual(flatten([[[4,3,2],2,[4,3]],45,65,43,[4,5]]), [4,3,2,2,4,3,45,65,43,4,5])
        self.assertEqual(flatten([[15,4,3],[32,243],[432,234,542]]), [15,4,3,32,243,432,234,542])
        self.assertEqual(flatten([5,4,3,5,6,5,4]), [5,4,3,5,6,5,4])
        self.assertEqual(flatten( [3,4,5,[(43),(23,53,234),(32)],32,[[123],32]] ),[3,4,5,43,23,53,234,32,32,123,32])

if __name__ == '__main__':
    unittest.main()
