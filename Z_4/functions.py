#Odnosnie 3.5
def miarka(height):
    '''
    Rozwiazanie zadanie 3.5 z poprzedniego zestawu ale w postaci funkcji
    @param hegith: int dlugosc miarki
    @return string: Zwraca miasrke w postacji pelnego stringu
    '''
    width = len(str(long))
    if width == 1:
        string = ' . . . . '.join('|' for x in range(long+1))
        string += '\n'
        string += '         '.join(str(x) for x in range(10))
    if width == 2:
        string = ' . . . '.join('|' for x in range(long+1))
        string += '\n'
        string += '       '.join(str(x) for x in range(10))
        string+= '      '
        string += '      '.join(str(x) for x in range(10,long+1))
    elif width == 3:
        string = ' . . . '.join('|' for x in range(long+1))
        string += '\n'
        string += '       '.join(str(x) for x in range(10))
        string += '      '
        string += '      '.join(str(x) for x in range(10,100))
        string += '     '
        string += '     '.join(str(x) for x in range(100,long+1))
    return string

#Odnosnie 3.6
def prostokat(height,weight):
    '''
    Rozwiazanie zadania 3.6 w postaci fukcji z popredniego zestawu
    @param height: int - dlugosc boku
    @param width: int szerokosc
    @return string: Zwraca prostokat w postaci stringu
    '''
    prostokat = ' - - - '.join('+' for i in range(0,weight+1))
    for i in range(0,height):
        prostokat += '\n'
        prostokat+= '       '.join('|' for i in range(0,weight+1))
        prostokat += '\n'
        prostokat += ' - - - '.join('+' for i in range(0,weight+1))
    return prostokat

#4.3
def factorial(n):
    '''
    Iteracyjna wersja funkcji obliczajacej silnie
    @param n: int - silnia z liczby
    @return int: obliczona wartosc
    '''
    result = 1
    while n > 0:
        result*= n
        n-=1
    return result

#4.4
def fibonaci(n):
    '''
    Zwraca n-ty wyraz fibonaciego
    @param n: int - wyraz fibonaciego
    @return int: wartosc n-tego wyrazu fibonaciego
    '''
    if n == 1 or n ==2:
        return 1
    a,b = 1, 1
    i = 2
    while i < n :
        c = a+b
        b = a
        a = c
        i += 1
    return a

#4.5
def odwracanieIteraccja(lista,left,right):
    '''
    funkcja odwracającą kolejność elementów na liście od numeru left do right
    włącznie. Lista jest modyfikowana w miejscu (in place)
    Wersja iteracyjna
    @param lista: lista elementow
    @param left lewy indeks
    @param right prawy indeks
    '''
    while left<=right:
        tmp = lista[left]
        lista[left] = lista[right]
        lista[right] = tmp
        left += 1
        right -= 1

def odwracanieRekurencja(lista,left,right):
    '''
    funkcja odwracającą kolejność elementów na liście od numeru left do right
    włącznie. Lista jest modyfikowana w miejscu (in place)
    Wersja rekurencyjna
    @param lista: lista elementow
    @param left lewy indeks
    @param right prawy indeks
    '''
    if left != right and left < right:
        tmp = lista[left]
        lista[left] = lista[right]
        lista[right] = tmp
        left+=1
        right-=1
        odwracanieRekurencja(lista,left,right)

#4.6
def help(sequence):
    '''
    Funkcja pomocnicza do zadania 4.6
    @sequence list: lista
    @suma int: Do tej wartosci sumujemy wartosci na zagniezdrzonej liscie
    '''
    suma = 0
    if  isinstance(sequence,(list,tuple)):  
        for i in sequence:
            suma += help(i)
    else:
        return sequence
    return suma


def sum_sequence(sequence):
    '''
    Funkcja obliczajaca sume liczb zawartych w sekwencji
    @param sequence: Pobiera sekwencje liczb lub sekwencje list
    @return int: suma liczb na liscie
    '''
    return help(sequence)

#4.7
def helpver(seq,lista):
    '''
    Funkcja pomocnicza do zadanie 4.7
    @param seq: lista  wartosci lub pojedyncza wartosc
    @param lista: nowa lista ktora bedzie wynikiem funkcji
    '''
    if isinstance(seq,(list,tuple)):
        for i in seq:
            helpver(i,lista)
    else:
        lista.append(seq)
def flatten(sequence):
    '''
    Funkcja splaszczajaca
    @param sequence sekwencja liczb lub zagniezdrzonych lista
    @return list: lista liczb zawartych w wejsciowej lisice
    '''
    lista = []
    helpver(sequence,lista)
    return lista

