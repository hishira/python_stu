## Zadanie 4
Autor: Michal Skorupa
Data: 28.10.2018
Kompilacja: Prosze kompilowac w pythonie 3

-Zadania od 4.2 do 4.7 napisane sa w pliku functions.py. Modul testujacy
funkcje od 4.3-4.7 to test.py.

-W zadaniu 4.5 funkcja odwracanieIteraccja() przedstawia wersje iteracyjna,
natomiast odwracanieRekurencja() wersje rekurencyjna

-W zadaniach 4.6,4.7 funkcje help() oraz helver() sa to funkcje pomocnicze.
Funkcja obliczajaca sume liczb zawartych w sekwencji to sum_sequence()
