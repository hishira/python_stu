def miarka2(long):
    width = len(str(long))
    if width == 1:
        string = ' . . . . '.join('|' for x in range(long+1))
        string += '\n'
        string += '         '.join(str(x) for x in range(10))
    if width == 2:
        string = ' . . . '.join('|' for x in range(long+1))
        string += '\n'
        string += '       '.join(str(x) for x in range(10))
        string+= '      '
        string += '      '.join(str(x) for x in range(10,long+1))
    elif width == 3:
        string = ' . . . '.join('|' for x in range(long+1))
        string += '\n'
        string += '       '.join(str(x) for x in range(10))
        string += '      '
        string += '      '.join(str(x) for x in range(10,100))
        string += '     '
        string += '     '.join(str(x) for x in range(100,long+1))
    return string

print(miarka2(16))
