from main import roman2int
import unittest

class Test(unittest.TestCase):

    def test_oneNumber(self):
        self.assertEqual(roman2int('CD'),400)

    def testOneToTen(self):
        self.assertEqual(roman2int('I'),1)
        self.assertEqual(roman2int('II'),2)
        self.assertEqual(roman2int('III'),3)
        self.assertEqual(roman2int('IV'),4)
        self.assertEqual(roman2int('V'),5)
        self.assertEqual(roman2int('VI'),6)
        self.assertEqual(roman2int('VII'),7)
        self.assertEqual(roman2int('VIII'),8)
        self.assertEqual(roman2int('IX'),9)
        self.assertEqual(roman2int('X'),10)

    def test_moreNumber(self):
        self.assertEqual(roman2int('I'),1)
        self.assertEqual(roman2int('XI'),11)
        self.assertEqual(roman2int('C'),100)
        self.assertEqual(roman2int('MMM'),3000)

    def test_onemoreTime(self):
        self.assertEqual(roman2int('XXX'),30)
        self.assertEqual(roman2int('L'),50)
        self.assertEqual(roman2int('CCC'),300)
        self.assertEqual(roman2int('XCVIII'),98)
        self.assertEqual(roman2int('LXXXIV'),84)
        self.assertEqual(roman2int('CDXCIX'),499)

    def testOneMoreTime(self):
        self.assertEqual(roman2int('MCDLVI'),1456)
        self.assertEqual(roman2int('MMCCCXLIV'),2344)
        self.assertEqual(roman2int('MMMCCXXXII'),3232)
        self.assertEqual(roman2int('CDXXIII'),423)
        self.assertEqual(roman2int('DCLXVI'),666)
        self.assertEqual(roman2int('LXXXIX'),89)

    def testWikipedia(self):
        self.assertEqual(roman2int('IV'),4)
        self.assertEqual(roman2int('VII'),7)
        self.assertEqual(roman2int('XIX'),19)
        self.assertEqual(roman2int('XL'),40)
        self.assertEqual(roman2int('XCV'),95)
        self.assertEqual(roman2int('CM'),900)
        self.assertEqual(roman2int('MXXV'),1025)
        self.assertEqual(roman2int('MCMXCV'),1995)
        self.assertEqual(roman2int('MM'),2000)
        self.assertEqual(roman2int('MCMLVI'),1956)
        self.assertEqual(roman2int('MMXI'),2011)
        self.assertEqual(roman2int('MMMDCCCLXXXVIII'),3888)

unittest.main()
