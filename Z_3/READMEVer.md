# Zadanie 3
 Michał Skorupa </br>
 Data: 22.10.2018 </br>
 Zadania: Zestaw 3 </br>
 Programu znajdujące się w folderze proszę uruchamiać w pythonie 3

## Zadanie 3.1
W poniższych kodach błędnym kodem jest:
```python
  for i in "qwerty": if ord(i) < 100: print i
oraz:
  for i in "axby": print ord(i) if ord(i) < 100 else i
```
## Zadanie 3.2
  ```python
  -x,y = 1,2,3
  #Za dużo wartości po stronie prawej więc wyskoczy wyjątek

  -X = 1,2,3; X[1] = 4
  #Obiekty w krotce są niezmienne, czyli nie można przypisać innej wartości do wartości
  #znajdującej się już w krotce(tuple)

  -X = [1,2,3]; X[3] = 4
  #Wyskoczy IndexError ponieważ lista X zawiera indexy 0,1,2

  -X = "abc" ; X.append("d")
  #String nie posiada metody append

  -map(pow,range(8))
  #Funkcja pow przyjmuje 2 argumenty, przykładowe wywołanie funkcji:
  map(lambda x: pow(x,c),range(8))
  #gdzie 'c' jest to potęga do której chcemy podnieść
  ```
## Zadania 3.3, 3.5, 3.6, 3.8, 3.9
  Powyższe zadania są zapisane w jednym pliku main.py, podczas wywołania programu zostaną
  uruchomione po kolei.

## Zadanie 3.4
  Aby nie rozpraszać wpisywaniem cyfr podczas uruchamiania zadań powyższe zadanie zostało
  umieszczone w osobnym pliku number.py

## Zadanie 3.10

 Funkcja roman2int() znajduję się na końcu pliku main.py oraz ma za zadanie
 zkonwertować liczbę zapisaną w systemie rzymskim na nasz system arabski.
 Klasa testująca podana funkcje znajduję się w pliku test.py
