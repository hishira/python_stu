if __name__ =='__main__':
    #3.3
    print('Zadanie 3.3: ')
    lista = [x for x in range(0,31) if x%3!=0]
    for i in lista:
        print(i)

    #3.5
    print()
    print('Zadanie 3.5: ')
    def miarka2(long):
        width = len(str(long))
        if width == 1:
            string = ' . . . . '.join('|' for x in range(long+1))
            string += '\n'
            string += '         '.join(str(x) for x in range(10))
        if width == 2:
            string = ' . . . '.join('|' for x in range(long+1))
            string += '\n'
            string += '       '.join(str(x) for x in range(10))
            string+= '      '
            string += '      '.join(str(x) for x in range(10,long+1))
        elif width == 3:
            string = ' . . . '.join('|' for x in range(long+1))
            string += '\n'
            string += '       '.join(str(x) for x in range(10))
            string += '      '
            string += '      '.join(str(x) for x in range(10,100))
            string += '     '
            string += '     '.join(str(x) for x in range(100,long+1))
        return string
    print(miarka2(16))


    #3.6
    print()
    print('Zadanie 3.6: ')
    def prostokat(height:int,weight:int):
        prostokat = ' - - - '.join('+' for i in range(0,weight+1))
        for i in range(0,height):
            prostokat += '\n'
            prostokat+= '       '.join('|' for i in range(0,weight+1))
            prostokat += '\n'
            prostokat += ' - - - '.join('+' for i in range(0,weight+1))
        print(prostokat)
    prostokat(10,16)

    #3.8
    print()
    print('Zadanie 3.8: ')
    lista1 = [1,2,4,3,5,456744,456,34,23,523,34,52,4]
    lista2 = [4,3,5,456744,456,4,2,567,86,45,754]
    #a
    print('a) ')
    print(lista1)
    print(lista2)
    print('Lista elementow występująca w obu powyzszych',set(lista1).intersection(set(lista2)))
    #b
    print('b)')
    print('Lista wszystkich elementow z obu sekwencji bez powtorzen',set(lista1).union(set(lista2)))

    #3.9
    print()
    print('Zadanie 3.9: ')
    def sum_of_sequence(lista):
        return [sum(i) for i in lista]
    print('Dla', [[],[4],(1,2),[3,4],(5,6,7)])
    print('Mamy: ',sum_of_sequence([[],[4],(1,2),[3,4],(5,6,7)]))
    print('Dla', [(),(),(1,2,3),[432],[32,43,243,53],(4233,342,234,42)])
    print('Mamy: ',sum_of_sequence([(),(),(1,2,3),[432],[32,43,243,53],(4233,342,234,42)]))
    print('Dla', [[43,43,23],(43,23),[4],[3],(43243,43),[-45,-45,-45],[4,5,3,4,5]])
    print('Mamy: ',sum_of_sequence([[43,43,23],(43,23),[4],[3],(43243,43),[-45,-45,-45],[4,5,3,4,5]]))

#3.10
roman = {
        'I':(1,1),
        'IV':(4,2),
        'V':(5,3),
        'IX':(9,4),
        'X':(10,5),
        'XL':(40,6),
        'L':(50,7),
        'XC':(90,8),
        'C':(100,9),
        'CD':(400,10),
        'D':(500,11),
        'CM':(900,15),
        'M':(1000,14)
    }
def roman2int(roman_number):
    i = 0
    suma = 0
    flag = True
    while i < len(roman_number) :
        if i+1 < len(roman_number) and ( roman[roman_number[i]][1] < roman[roman_number[i+1]][1]):
            suma += roman[str(roman_number[i]+(roman_number[i+1]))][0]
            i+=2
        else:
            suma+= roman[roman_number[i]][0]
            i+=1
    return suma if len(roman_number) > 1 else roman[roman_number][0]
