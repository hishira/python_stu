'''
HeapSort - sortowanie przy uzyciu kopca
i = rodzic
2*i + 1 - lewy syn
2*i + 2 - prawy syn
(i-1)/2 - rodzic
'''
import datetime
from random import randint

def heapsort(L:list,len:int,heap_count):
    start = 1
    rodzic = (start-1)//2
    length = len
    while start < length:
        start_tmp = start
        rodzic_tmp = rodzic
        while rodzic_tmp >= 0 and L[rodzic_tmp] < L[start_tmp]:
              tmp = L[rodzic_tmp]
              L[rodzic_tmp] = L[start_tmp]
              L[start_tmp] = tmp
              start_tmp = rodzic_tmp
              heap_count[0] += 1
              rodzic_tmp = (start_tmp-1) // 2
        start += 1
        rodzic = (start-1)//2
    start = len - 1
    while start >=0:
        tmp = L[start]
        poczatek = 0
        L[start] = L[poczatek]
        L[poczatek] = tmp
        heap_count[0] += 1
        syn = 2 * poczatek + 1
        while syn + 1 < start:
            if syn + 1 < start and L[syn] < L[syn+1]:
                syn = syn + 1
            if L[poczatek] >= L[syn]:
                break
            tmp = L[syn]
            L[syn] = L[poczatek]
            L[poczatek] = tmp
            poczatek = syn
            heap_count[0] += 1
            syn = 2*poczatek +1
        start -=1
    return L

def swap(L, left, right):
    """Zamiana miejscami dwóch elementów."""
    # L[left], L[right] = L[right], L[left]
    item = L[left]
    L[left] = L[right]
    L[right] = item

def bubblesort(L, left, right):
    for i in range(left, right):
        for j in range(left, right):
            if L[j] > L[j+1]:
                swap(L, j+1, j)

def quicksort(L, left, right,quick_count):
    if left >= right:
        return
    swap(L, left, (left + right) // 2)   # element podziału
    quick_count[0] += 1
    pivot = left                      # przesuń do L[left]
    for i in range(left + 1, right + 1):   # podział
        if L[i] < L[left]:
            pivot += 1
            swap(L, pivot, i)
    swap(L, left, pivot)     # odtwórz element podziału
    quick_count[0] += 1
    quicksort(L, left, pivot - 1,quick_count)
    quicksort(L, pivot + 1, right,quick_count)

if __name__ == '__main__':
    heap_count = []
    quick_count = []
    heap_count.append(0)
    quick_count.append(0)
    lista = []
    for i in range(1000000):
        lista.append(randint(-100000,100000))
    length = len(lista)
    lista = sorted(lista,reverse=True)
    tmp = lista.copy()
    #buble_start = datetime.datetime.now()
    #bubblesort(lista,0,length-1)
    #buble = datetime.datetime.now() - buble_start
    quick_start = datetime.datetime.now()
    quicksort(lista,0,length-1,quick_count)
    quick = datetime.datetime.now() - quick_start
    heap_start = datetime.datetime.now()
    heapsort(tmp,length,heap_count)
    heap = datetime.datetime.now() - heap_start
    #print('Bublesort:', buble)
    print('Quicksort:',quick)
    print('Heapsort:',heap)
    print(tmp == lista)
    print('Ilosc zamian heap:',heap_count[0])
    print('Ilosc zamian quick:',quick_count[0])
