# Zadanie 11
Michał Skorupa <br>
Data: 23.10.2018 <br>
Zadania z zestawu 11, prszę odpalac z pythonem 3

# Zadanie 11.1
Przygotować moduł Pythona z funkcjami tworzącymi listy liczb całkowitych do sortowania. Przydatne są m.in. następujące rodzaje danych: <br>
(a) różne liczby int od 0 do N-1 w kolejności losowej, <br>
(b) różne liczby int od 0 do N-1 prawie posortowane (liczby są blisko swojej prawidłowej pozycji), <br>
(c) różne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejności,<br>
(d) N liczb float w kolejności losowej o rozkładzie gaussowskim, <br>
(e) N liczb int w kolejności losowej, o wartościach powtarzających się, należących do zbioru k elementowego (k < N, np. k*k = N).

### Wersja 1 podpunktu d
```python
def four(long:int):
    '''
    N liczb float w kolejnosci losowej o rozkladzie gaussowskim,
    @param long: Dlugosc listy
    '''
    lista = []
    while len(lista) != long:
        u1,u2 = random(),random()
        y1 = sqrt(-2*log(u1,e))*cos(2*pi*u2)
        y2 = sqrt(-2*log(u1,e))*sin(2*pi*u2)
        lista.extend([y1,y2])
    return lista

```
## ZADANIE 11.5
Zaimplementować w języku Python algorytm sortowania, który nie został omówiony na wykładzie. Dołączyć krótką informację o cechach algorytmu.<br>
Zaimplementowano sortowanie heapsort<br>
<hr>
#### 1
Dla porównania. Czas dla sortowania 10000 elementów z przedzialu -100,100 np:<br>
BubleSort: <b>0:00:54.639028</b> <br>
HeapSort:  <b>0:00:00.208921</b>
<br>
<hr>
#### 2
Porównalem tez z quicksort dla 100000 elementowego z tego samego przedzialu: <br>
Quicksort: <b>0:00:08.244038</b> <br>
Heapsort: <b>0:00:02.714320</b>
<hr>
#### 3
Porównalem tez z quicksort dla 100000 elementowego z przedzialu -100000,100000 : <br>
Quicksort: <b>0:00:01.868576</b> <br>
Heapsort: <b>0:00:02.805471</b>
<hr>
#### 4
Porównalem tez z quicksort dla 1000000 elementowego z przedzialu -100000,100000 : <br>
Quicksort: <b>0:00:23.688759</b> <br>
Heapsort: <b>0:00:35.742485</b>
<br>
<br>
<hr>

Różnica w czasach może być spowodowana tym że podaczas algorytmu heapsort występuje wiecej zamian(swap) wartości niż w przypadku quicksort.<br>Dla 1 000 000 wartosci z przedzialu -100000,100000 występuje:<br>
Heapsort: 18 328 449 zamian<br>
QuickSort: 1 622 272 zamian
