'''
W module tym opiszemy 5 metod których zadaniem będzie stworzenie odpowiednich
list które będzie mozna posortowac

(a) rozne liczby int od 0 do N-1 w kolejnosci losowej,
(b) rozne liczby int od 0 do N-1 prawie posortowane (liczby sa blisko swojej prawidlowej pozycji),
(c) rozne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejnosci,
(d) N liczb float w kolejnosci losowej o rozkladzie gaussowskim,
(e) N liczb int w kolejności losowej, o wartościach powtarzających się, należących do zbioru k elementowego (k < N, np. k*k = N).
'''

from random import randint,random,choice
from math import sqrt,log,e,pi,sin,cos

def first(long:int) -> list:
    '''
    rozne liczby int od 0 do N-1 w kolejnosci losowej
    @param long: liczby od 0 do long-1
    '''
    lista = []
    drawn = [] #wylosowane liczby
    while len(lista) != long:
        b = randint(0,long-1)
        if b not in drawn:
            lista.append(b)
            drawn.append(b)
    return lista

def second(long:int) -> list:
    '''
    rozne liczby int od 0 do N-1 prawie posortowane (liczby sa blisko swojej prawidlowej pozycji),
    @param long: liczby od 0 do long-1
    '''
    lista = sorted([x for x in range(long)])
    def swap3element(L:list,a:int,b:int,c:int):
        tmp,tmp2 = L[b],L[a]
        L[c],L[a],L[b] = tmp2,tmp,L[c]

    def swap2element(L:int,a:int,b:int):
        L[a],L[b] = L[b],L[a]
    i = 0
    while i < long:
        if i+1<long and i+2 < long:
            swap3element(lista,i,i+1,i+2)
        elif i+1 < long:
            swap2element(lista,i,i+1)
        i = i+3
    return lista

def third(long:int):
    '''
    rozne liczby int od 0 do N-1 prawie posortowane (liczby sa blisko swojej prawidlowej pozycji),
    @param long: liczby od 0 do long-1
    '''
    lista = sorted([x for x in range(long)],reverse=True)
    def swap3element(L:list,a:int,b:int,c:int):
        tmp,tmp2 = L[b],L[a]
        L[c],L[a],L[b] = tmp2,tmp,L[c]

    def swap2element(L:int,a:int,b:int):
        L[a],L[b] = L[b],L[a]
    i = 0
    while i < long:
        if i+1<long and i+2 < long:
            swap3element(lista,i,i+1,i+2)
        elif i+1 < long:
            swap2element(lista,i,i+1)
        i = i+3
    return lista

def four(long:int):
    '''
    N liczb float w kolejnosci losowej o rozkladzie gaussowskim,
    @param long: Dlugosc listy
    '''
    lista = []
    lista2 = []
    while len(lista) != long:
        u1,u2 = random(),random()
        y1 = sqrt(-2*log(u1,e))*cos(2*pi*u2)
        y2 = sqrt(-2*log(u1,e))*sin(2*pi*u2)
        lista2.append([u1])
        lista.extend([y1,y2])
    return lista

def fifth(long:int):
    '''
    N liczb int w kolejności losowej, o wartościach powtarzających się, należących do zbioru k elementowego (k < N, np. k*k = N).
    @param long: Dlugosc listy
    '''
    lista = []
    k = int(sqrt(long))
    listLengthK = [randint(0,1000) for _ in range(0,k+1)]
    while len(lista) != long:
        lista.append(choice(listLengthK))
    return lista


if __name__ == '__main__':
    print(first(10))
    print(second(20))
    print(third(20))
    print(fifth(20))
