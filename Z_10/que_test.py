import unittest
from priorityqueue import PriorityQueue
from random import randint


class TestQueueClass(unittest.TestCase):
    '''
    Klasa testujaca modul priorityqueue, a w szczegolnosci klase PriorityQueue
    '''

    def setUp(self):
        '''
        Ustawienie poczatkowych wartosci
        '''
        self.pqueue1 = PriorityQueue()
        self.pqueue2 = PriorityQueue()
        self.pqueue3 = PriorityQueue()
        #Wylosowanie piorytetow dla poszczegolnych wartosci dla 3 kolejek

        #Pierwsza kolejka
        self.rand1 = [randint(-100,100) for x in range(0,10)]
        number1 = [randint(-100,100) for x in range(0,10)]
        #Druga kolejka
        self.rand2 = [randint(-100,100) for x in range(0,20)]
        number2 =[randint(-100,100) for x in range(0,20)]
        #Trzecia kolejka
        self.rand3 = [randint(-100,100) for x in range(0,30)]
        number3 = [randint(-100,100) for x in range(0,30)]
        for i in range(0,10):
            self.pqueue1.insert(number1[i],self.rand1[i])
        for i in range(0,20):
            self.pqueue2.insert(number2[i],self.rand2[i])
        for i in range(0,30):
            self.pqueue3.insert(number3[i],self.rand3[i])
        self.rand1.sort()
        self.rand2.sort()
        self.rand3.sort()

    def test_insertMethod(self):
        '''
        Testowanie metody insert bedzie polegalo na tym czy odpowiednio pierwszy
        element w naszym kopcu czyli kolejce pioretytetowej ma element nejwiekszy
        z posrod wszystkich elementow czyli majac zapisane tablice mozemy porownac
        z posortowanej tablicy z elementem na poczatku kopca
        '''
        self.assertEqual(self.pqueue1[0],self.rand1[-1])
        self.assertEqual(self.pqueue2[0],self.rand2[-1])
        self.assertEqual(self.pqueue3[0],self.rand3[-1])

    def test_removeMethod(self):
        '''
        Jesli usuwamy, to najwiekszy piorytet bedzie mial element ktory po
        posortowaniu jest drugi w kolejnosci, czyli jesli pamietamy listy
        piorytetow przydzielonych do wartosci, to wystarczy sprawdzic czy po
        usunieciu na poczatku bedzie stala wartosc druga co do wielkosci z
        posortowanej tablicy
        '''

        self.pqueue1.remove()
        self.pqueue2.remove()
        self.pqueue3.remove()
        self.assertEqual(self.pqueue1[0],self.rand1[-2])
        self.assertEqual(self.pqueue2[0],self.rand2[-2])
        self.assertEqual(self.pqueue3[0],self.rand3[-2])

    def test_exceptionRemove(self):
        '''
        Sprawdzamy czy proba usuniecia elementu z pustej kolejki wywola wyjatek
        '''
        kopczyk = PriorityQueue()
        with self.assertRaises(Exception):
            kopczyk.remove()

    def test_exceptionInsert(self):
        '''
        Sprawdzamy czy proba wsadzenia elementu do pelnej kolejki wywola wyjatek
        nasza kolejka ma maksymalna dlugosc 100 elementow
        '''
        kopczyk = PriorityQueue()
        with self.assertRaises(Exception):
            for i in range(0,200):
                kopczyk.insert(randint[0,100],randint[0,1000])

if __name__ == '__main__':
    unittest.main()
