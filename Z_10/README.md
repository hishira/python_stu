# Zadania 10
Autor : Michal Skorupa
Zrobione zadania:
  - 10.3
    - Funkcje wyjasnione w komentarzach
    - Napisany modul testujacy klase Stack
    - Dodalem kilka metod pomocniczych do klasy Stack, aby ulatwic testowanie
  - 10.5
    - Funkcje skomentowane
    - Tak jak w zadaniu 10.3 napisany modul testujacy klase PriorityQueue
    - W implementacji kolejki pioretytetowej uzyto kopca

## ZADANIE 10.3 (STACK)
Stworzyć implementację tablicową stosu do przechowywania bez powtórzeń liczb całkowitych od 0 do size-1. Powtarzająca się liczba ma być ignorowana bez żadnej akcji (inne podejście to wyzwolenie wyjątku). Wskazówka: stworzyć drugą tablicę, w której 0 lub 1 na pozycji i oznacza odpowiednio brak lub istnienie liczby i na stosie.

## ZADANIE 10.5 (PRIORITYQUEUE)
  1. Poprawić metodę remove() w pythonowej implementacji kolejki priorytetowej tak, aby uniknąć kosztownego pobierania elementu ze środka listy.

  2. Poprawić metodę remove() w tablicowej implementacji kolejki priorytetowej, aby w przypadku pustej kolejki generowała wyjątek.

  3. Poprawić metodę insert() w tablicowej implementacji kolejki priorytetowej, aby w przypadku przepełnienia kolejki generowała wyjątek.

Napisany zostal tez modul testujacy klase PriorityQueue gdzie testujemy metody
insert remove, oraz generowanie wyjatkow. Testowanie metod insert oraz remove
polega na tym ze na poczatku generuemy sobie liste dowolnych piorytetow oraz
liczb, umieszczamy je w kolejce a nastepnie sortujemy liste piorytetow.
Sprawdzanie polega na tym czy pierwszy element w kolejce ma najwiekszy
piorytet. Czyli porownujemy go z ostatnim elementem posortowanej listy.
W przypadku metody remove dziala to podobnie tylko ze porownujemy z elementem
drugim od konca
