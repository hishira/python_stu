import unittest
from random import randint
from stack import Stack

def initializeStack(size:int,*values) ->Stack:
    '''
    Funkcja pomocnicza, inicjalizuje nam stos podanymi wartosciami
    @param size: int value, size of stack
    @param *values: values to put in  stack
    @return Stack: return Stack with size and fill with values
    '''
    stack = Stack(size)
    for v in values:
        stack.push(v)
    return stack

def initiaStackWithRandom(size:int) -> tuple:
    '''
    Podobna do funkcji initializeStack, lecz inicjalizuje stos liczbami rand
    @param size: stack size
    @return tuple: zwraca (Stos, list) lista jest do porownywania
    '''
    random = [randint(-1000,1000) for i in range(0,size)]
    random = list(set(random))
    stack = Stack(size)
    for i in random:
        stack.push(i)
    arr = random.copy()
    return (stack,arr)


class testSctack(unittest.TestCase):
    '''
    Klasa testujaca klase Stack z zadania 10.3
    '''
    def setUp(self):
        '''
        Zainicjalizowanie paru stosow, oraz list z ktorymi te stosy bedziemy
        porownywac
        '''
        self.stack = initializeStack(10, 5,4,3,2,2,2,2,2,1,3)
        self.array = [5,4,3,2,1]
        self.stack_1 = initializeStack(5, 4556,32,-23,12,12,12,12,32,32,32)
        self.array_1 = [4556,32,-23,12]
        self.stack_2 = initializeStack(5, 2)
        self.array_2 = [2]
        self.stack_3,self.array_3 = initiaStackWithRandom(20)

    def test_eq_function(self):
        '''
        Testuje czy stos równa się liscie z tymi samymi elementami w tej samej
        kolejnosci co na stosie
        '''
        self.assertEqual(repr(self.stack),str(self.array))
        self.assertEqual(repr(self.stack_1),str(self.array_1))
        self.assertEqual(repr(self.stack_2),str(self.array_2))
        self.assertEqual(repr(self.stack_3),str(self.array_3))

    def test_notPassSameVal(self):
        '''
        Sprawdza czy wartosci na liscie sie nie powtarzaja
        '''
        stack = Stack(5)
        arr = []
        for i in range(0,5):
            stack.push(i)
            arr.append(i)

        self.assertTrue(self.stack == self.array)
        self.assertTrue(self.stack_1 ==[4556,32,-23,12] )
        self.assertTrue(stack == arr)

    def test_pushMethod(self):
        '''
        Funkcja testujaca metode push klasy Stack  porownujaca ja do
        odpowiedniej list
        '''
        self.stack.push(20)
        self.array.append(20)
        self.stack_1.push(-3123)
        self.array_1.append(-3123)
        self.stack_2.push(132)
        self.stack_2.push(13322)
        self.array_2.append(132)
        self.array_2.append(13322)
        self.assertTrue(self.stack == self.array)
        self.assertTrue(self.stack_1 == self.array_1)
        self.assertTrue(self.stack_2 == self.array_2)


if __name__ == '__main__':
    unittest.main()
