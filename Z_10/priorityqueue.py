class PriorityQueue():
    '''
    Klasa reprezentuja kolejke pioretytetowa zaimplementowana za pomoca
    kopca w tablicy
    jesli rodzic ma indeks i to synowie maja 2i+1 oraz 2i+2
    zakladamy ze kolejka bedzie miala maksymalna pojemnosc 100 elementow
    '''
    class Storage():
        '''
        Klasa pomocnicza przechowujaca element oraz jego piorytet
        '''
        def __init__(self,value,priority):
            self.value = value
            self.priority = priority

        def __repr__(self):
            '''
            Funkcja pomocnicza przy wyswietlaniu wartosci w metodzie print
            '''
            return str((self.value,self.priority))

    def __init__(self):
        '''
        Konstruktor kolejki pioretytetowej  na bazie tablicy
        '''
        self.items = [None] * 100
        self.last = 0

    def __getitem__(self,index):
        return self.items[index].priority
    def __str__(self):
        '''
        Zwraca kolejke w postaci napisu
        '''
        return str([x for x in self.items if x is not None])

    def is_empty(self) -> bool:
        '''
        Sprawdza czy kolejka jest pusta
        @return bool: True jesli jest pusta,False jesli nie jest pusta
        '''
        return True if len(self.items) == 0 else False

    def insert(self,value,priority):
        '''
        Metoda ta wprowadza, dodaje wartosc do kolejki z pioretytetami
        @param value: wartosc przechowywana w kolejce
        @param priority: piorytet wartosci przechowywanej w kolejce
        @raise BaseException: zwraca wyjatek jesli kolejka jest pelna
        '''
        try:
            if self.last == 100:
                raise BaseException("Kolejka jest pelna")
        except BaseException as error:
            print(error.args)
            raise
        node = self.Storage(value,priority)
        i = self.last
        self.items[i]  = node
        self.last += 1
        parrent = (i-1)//2
        while  i > 0 and self.items[parrent].priority < self.items[i].priority :
            tmp = self.items[i]
            self.items[i] = self.items[parrent]
            self.items[parrent] = tmp
            i = parrent
            parrent = (parrent-1)//2

    def remove(self):
        '''
        Metoda usuwa wartosc z kolejki z najwiekszym piorytetem w postaci
        nie klasy ale samej wartosci przechowywanej w objekcie Storage
        @return value: Storage.value
        @raise Exception: Jesli kolejka jest pusta zwraca wyjatek
        '''
        try:
            if self.last == 0:
                raise Exception('Kolejka jest pusta')
        except Exception:
            print('Kolejka jest pusta nie da sie usunac elementu')
            raise
        last = self.last-1
        self.last -= 1
        self.items[0] = self.items[last]
        data = self.items[0].value
        now = 0
        child = 1
        while (now < self.last):
            if child +1 < self.last and self.items[child+1].priority > self.items[child].priority:
                child += child
            elif child < self.last and self.items[now].priority >= self.items[child].priority:
                break
            tmp = self.items[now]
            self.items[now] = self.items[child]
            self.items[child] = tmp
            now = child
            child = 2*child+1
        return data

if __name__ == '__main__':
    kopiec = PriorityQueue()
    kopiec.insert(20,5)
    kopiec.insert(100,1)
    kopiec.insert(4,450)
    kopiec.insert(45,323)
    kopiec.insert(43,323)
    kopiec.remove()
    kopiec.insert(4,10000)
    print(kopiec)
    kopiec.insert(3,2133)
    kopiec.remove()
    print(kopiec)
    kopczyk = PriorityQueue()
    kopczyk.remove()
