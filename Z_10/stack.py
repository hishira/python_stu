
class Stack():
    '''Klasa stos pierwsze wersja bez tej drugiej tablicy
        z troche większa iloscią metod
    '''
    def __init__(self,size:int):
        '''
        Konstruktor klasy stack
        '''
        self.items = [None] * size
        self.capacity = size
        self.fill = 0

    def __eq__(self,other) -> bool:
        '''
        Funckaj pomocnica do modulu testujacego klase Stack, bedziemy poprostu
        porownywali stos z tablica
        @param other:Musimy porownywac z tablica
        @return bool: Zwraca True jesli rowne, w przeciwnym wypadku False
        '''
        if not isinstance(other,(list,tuple)):
            return ValueError('Nie mozna porownac stosu do czegos innego jak lista')
        if isinstance(other,tuple):
            other = list(other)
        return [x for x in self.items if x is not None] == other

    def __str__(self) -> str:
        '''
        zwraca reprezentacje napisowa stosu czyli jak bedzie wygladal stos po wypisaniu
        go w princie
        @return str: reprezentacja napisowa stosu
        '''
        return str([x for x in self.items if x is not None])

    def __repr__(self):
        '''
        Zwraca reprezentacje stosu, pomocnicza
        '''
        return str([x for x in self.items if x is not None])

    def __len__(self) -> int:
        '''
        Funkcja len dla naszej klasy
        @return int: dlugosc stosu, albo liczba przechowywanych elementow
        '''
        return self.fill

    def __getitem__(self,index) -> int:
        '''
        Metoda moze nie za bardzo potrzebna ale umozliwai podglad elementow
        na stosie po indexach
        @return int: wartosc przechowywana na stosie poda index
        '''
        if index >= self.fill or index < 0:
            raise IndexError('Podany index nie isnieje')
        return self.items[index]

    def is_empty(self) -> bool:
        '''Metoda sprawdza czy stos jest pusty
        @return bool: True jesli stos pusty, False w przeciwnym wypadku
        '''
        return True if not self.fill else False

    def if_full(self) -> bool:
        '''
        Metoda sprawdza czy stos jest pelny
        @return bool: True jesli jest pelny, jesli nie jest False
        '''
        return True if self.fill == self.capacity else False

    def push(self,data:int):
        '''
        Wprowadza wartosc na stos
        @param data: wartosc int ktora wprowadzamy na stos
        '''
        if data in self.items:
            pass
        else:
            if self.fill < self.capacity:
                self.items[self.fill] = data
                self.fill += 1
            else:
                print('Stos jest pelny, nie mozna wpisac nowej wartosci')

    def pop(self) -> int:
        '''Usuwa i zwraca ostatnia wartosc znajdujaca sie na stosie
        @return int: zwraca ostatnia wartosc na stosie
        '''
        if self.fill == 0:
            print('Stos jest pusty')
            pass
        self.fill -= 1
        data = self.items[self.fill]
        self.items[self.fill] = None
        return data
